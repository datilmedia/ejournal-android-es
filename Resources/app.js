var parse = require('parselogin');
var parseCloud = require('parsecloud');

parseCloudClient = new parseCloud.Client();

Titanium.UI.setBackgroundColor('#000');
 
var app = {};

app.isAndroid = (Titanium.Platform.osname == 'android');
app.platform = Titanium.Platform.osname;
app.version = Titanium.App.getVersion();
 
var lenguaje;

var totalPaginas = 0;

if( Ti.App.Properties.hasProperty( "lenguaje" )==0){
	lenguaje = 'es';
	Ti.App.Properties.setString('lenguaje', lenguaje);
};

var username = Ti.UI.createTextField({
	autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
	width: 300,
	height: 40,
	borderStyle: Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	top: '30%',
	font: {
    	font : 'Arial',
		fontSize : 14
	},
});

var password = Ti.UI.createTextField({
	autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
	borderStyle: Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	passwordMask: true,
	width: 300,
	height: 40,
	top: '33%',
	font: {
    	font : 'Arial',
		fontSize : 14
	},
});
		
var buttonEntrar = Titanium.UI.createButton({
	backgroundImage:'boton_azul.png',
	color:'white',
	width: 300,
	height: 40,
	top: '36%',
});

var buttonRecuperacion = Titanium.UI.createButton({
	backgroundImage: 'boton_azul.png',
	top: '60%',
	width: 300,
	height: 40,
	fontSize: 18,
	color: '#FFF',
});

var cargandoIngreso = Ti.UI.createView({
    backgroundColor: 'black',
    opacity: 0.75,
    height: '100%',
    width: '100%'
});

var logoGrupoSpurrier = Ti.UI.createImageView({
	image: 'logo-gs.png',
	width: 200,
	height: 43,
	bottom: '45',
	left: '40'
});

var labeleditores = Titanium.UI.createLabel({
	color: 'white',
	id: 'font_label_test',
	text: 'Walter Spurrier Baquerizo \n DIRECTOR \n\n Alberto Acosta Burneo \n EDITOR',
    font: {
    	font : 'Arial',
		fontSize : 16
	},
	height: 170,
	textAlign: 'center',
	top: "45%"
});

var labelDireccion = Titanium.UI.createLabel({
	color: 'white',
	id: 'direccion',
	text: 'Plaza Lagos, Edificio Mirador, oficina 2-1 \nKm. 6,5 Vía Puntilla – Samborondón, Guayaquil \nPBX (593-4) 500 9343 \nwww.grupospurrier.com \nInfo@grupospurrier.com',
	height: 120,
	bottom: 10,
	right: 40
})
 
var indicadorDeIngreso = Ti.UI.createActivityIndicator({
    style: Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
    font: {
    	font : 'Arial',
		fontSize : 24
	},
    color: '#FFF'
});
 
cargandoIngreso.add(indicadorDeIngreso);				

var cargandoDatos = Ti.UI.createView({
    backgroundColor: 'black',
    opacity: 0.75,
    height: '100%',
    width: '100%'
});
 
var indicadorDeCargaDeDatos = Ti.UI.createActivityIndicator({
    style: Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
    font : {
    	font : 'Arial',
		fontSize : 24
	},
    color: '#FFF'
});

var errorIngreso = Ti.UI.createView({
    backgroundColor: 'black',
    opacity: 0.75,
    height: '100%',
    width: '100%'
});

if (Ti.App.Properties.getString('lenguaje') == 'es'){
	username.hintText = 'Nombre de Usuario';
	password.hintText = 'Contraseña';
	buttonEntrar.title = 'Entrar';
	buttonRecuperacion.title = 'Cambie/Recupere su Contraseña';
	indicadorDeIngreso.message = 'Ingresando, espere por favor...'
	
} else {
	username.hintText = 'Username';
	password.hintText = 'Password';
	buttonEntrar.title = 'Login';
	buttonRecuperacion.title = 'Change/Recover your password';
	indicadorDeIngreso.message = 'Logging in, please wait...'
};			 
 
cargandoDatos.add(indicadorDeCargaDeDatos);

var win = Titanium.UI.createWindow({  
    title: '',
	backgroundColor:'transparent',
});

win.orientationModes = [ 
    Titanium.UI.PORTRAIT
];

win.setBackgroundGradient( {
	type: 'linear',
	colors: ['#10427a','#305790'],
	backFillStart: false
});

var image = Ti.UI.createImageView({
	top: "17%",
	image:'logo.png',
	width:254,
	height:78,
});

win.add(image);

win.add(logoGrupoSpurrier);

win.add(username);

win.add(password);

win.add(buttonEntrar);

win.add(buttonRecuperacion);

win.add(labeleditores);

win.add(labelDireccion);

buttonEntrar.addEventListener('click', function() {

	win.add(cargandoIngreso);
	indicadorDeIngreso.show();
	
	var nuevaVentana = Titanium.UI.createWindow({
		url:'vistas/inicio.js',
		title:'inicio',
		backgroundColor:'white',
		width: '100%',
		height: '100%',
	});
	
	var client = new parse.Client();
	
	//var whereClause = {
	//	"objectId" : "WG4n4qiZBK"
	//};
	client.get({
		className : 'Editions',
		payload : {
			"username" : username.value,
			"password" : password.value
			
		//	"username" : "test",
		//	"password" : "test"
		},
		success : function(user) {
			if (user.userAccessQuota > 0) {
				saveParams = {
					className: 'User', objectId: user.objectId,
					object: {'userAccessQuota': {'__op': "Increment", 'amount': -1}},
					sessionToken: user.sessionToken,
					success: function(savedUser) {
						win.remove(cargandoIngreso);
						nuevaVentana.open();
						indicadorDeIngreso.hide();
					},
					error: function(savedUser, error) {
						console.log('Error al administrar cupos de acceso.');
					}
				};
				parseCloudClient.update(saveParams);
			} else {
				indicadorDeIngreso.hide();
				win.remove(cargandoIngreso);
				alert('Ya no tiene accesos semanales disponibles. / You no longer have access permissions available.');
				//win.add(errorIngreso);
			}
		},
		error : function(response, xhr) {
			indicadorDeIngreso.hide();
			win.remove(cargandoIngreso);
			alert('Usuario o Password Incorrecto!');
		}
	});
});

buttonRecuperacion.addEventListener('click', function() {
	var ventanaRecuperacion = Titanium.UI.createWindow({
		url:'vistas/recuperacion.js',
		title:'recuperacion',
		backgroundColor:'white',
		width: '100%',
		height: '100%',
	});
	
	ventanaRecuperacion.open({transition:Ti.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT});
	
});

win.open();
